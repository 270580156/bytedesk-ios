//
//  BDVisitorInfoEditTableViewController.h
//  bytedesk-ui
//
//  Created by 宁金鹏 on 2022/1/7.
//  Copyright © 2022 KeFuDaShi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BDVisitorInfoEditTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
